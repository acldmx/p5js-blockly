import Blockly from "blockly";  
const view = {
  "type": "line",
  "message0": "绘制直线 x1 %1 y1 %2 x2 %3 y2 %4",
  "args0": [
    {
      "type": "input_value",
      "name": "X1",
      "check": "Number"
    },
    {
      "type": "input_value",
      "name": "Y1",
      "check": "Number"
    },
    {
      "type": "input_value",
      "name": "X2",
      "check": "Number"
    },
    {
      "type": "input_value",
      "name": "Y2",
      "check": "Number"
    }
  ],
  "inputsInline": true,
  "previousStatement": null,
  "nextStatement": null,
  "colour": 315,
  "tooltip": "绘制一条直线",
  "helpUrl": ""
}

const generator = function (block) {
  const x1 = Blockly.JavaScript.valueToCode(block, 'X1', Blockly.JavaScript.ORDER_ATOMIC);
  const y1 = Blockly.JavaScript.valueToCode(block, 'Y1', Blockly.JavaScript.ORDER_ATOMIC);
  const x2 = Blockly.JavaScript.valueToCode(block, 'X2', Blockly.JavaScript.ORDER_ATOMIC);
  const y2 = Blockly.JavaScript.valueToCode(block, 'Y2', Blockly.JavaScript.ORDER_ATOMIC);

  const code = `
    line(${x1}, ${y1}, ${x2}, ${y2});
  `;
  return code;
}

export default {
  name: 'line',
  view,
  generator,
}