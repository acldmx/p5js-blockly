import Blockly from "blockly";  
const view = {
  "type": "ellipse",
  "message0": "绘制圆形 %1 x坐标 %2 y坐标 %3 半径 %4",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "input_value",
      "name": "X",
      "check": "Number"
    },
    {
      "type": "input_value",
      "name": "Y",
      "check": "Number"
    },
    {
      "type": "input_value",
      "name": "RADIUS",
      "check": "Number"
    }
  ],
  "inputsInline": true,
  "previousStatement": null,
  "nextStatement": null,
  "colour": 230,
  "tooltip": "绘制圆形",
  "helpUrl": ""
};

const generator = function (block) {
  var value_x = Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC);
  var value_y = Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC);
  var value_radius = Blockly.JavaScript.valueToCode(block, 'RADIUS', Blockly.JavaScript.ORDER_ATOMIC);
  
  var code = `ellipse(${value_x}, ${value_y}, ${value_radius});`
  return code;
}

export default {
  name: 'ellipse',
  view,
  generator,
}