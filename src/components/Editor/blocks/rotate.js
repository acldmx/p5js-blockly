import Blockly from "blockly";
const view = {
  "type": "rotate",
  "message0": "以 X %1 Y %2 为中心, 旋转 %3 度 %4 %5",
  "args0": [
    {
      "type": "input_value",
      "name": "X"
    },
    {
      "type": "input_value",
      "name": "Y"
    },
    {
      "type": "input_value",
      "name": "文本",
      "check": "Number"
    },
    {
      "type": "input_dummy"
    },
    {
      "type": "input_statement",
      "name": "NAME"
    }
  ],
  "inputsInline": true,
  "previousStatement": null,
  "nextStatement": null,
  "colour": 230,
  "tooltip": "绘制圆弧",
  "helpUrl": ""
}

const generator = function (block) {
  var value_x = Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC);
  var value_y = Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC);
  var deg = Blockly.JavaScript.valueToCode(block, 'DEG', Blockly.JavaScript.ORDER_ATOMIC)

  const code = `
    push();
    translate(${value_x}, ${value_y});\n
    rotate(${deg} * PI / 180);\n
    pop();
  `;

  return code;
}

export default {
  name: 'rotate',
  view,
  generator,
}