import Blockly from "blockly";

const view = {
  "type": "random",
  "message0": "生成一个 %1 到 %2 之间的随机数",
  "args0": [
    {
      "type": "input_value",
      "name": "START_NUMBER",
      "check": "Number"
    },
    {
      "type": "input_value",
      "name": "END_NUMBER",
      "check": "Number"
    }
  ],
  "inputsInline": true,
  "output": null,
  "colour": 230,
  "tooltip": "生成一个随机数",
  "helpUrl": ""
}

const generator = function (block) {
  var start_number = Blockly.JavaScript.valueToCode(block, 'START_NUMBER', Blockly.JavaScript.ORDER_ATOMIC);
  var end_number = Blockly.JavaScript.valueToCode(block, 'END_NUMBER', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = `random(${start_number}, ${end_number})\n`
  return [code, Blockly.JavaScript.ORDER_NONE];;
}

export default {
  name: 'random',
  view,
  generator,
}