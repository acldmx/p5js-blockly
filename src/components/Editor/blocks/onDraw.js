import Blockly from "blockly";

const view = {
  "type": "setup",
  "message0": "更新 %1 %2",
  "args0": [
    {
      "type": "input_dummy",
      "align": "RIGHT"
    },
    {
      "type": "input_statement",
      "name": "CODE"
    }
  ],
  "colour": 230,
  "tooltip": "",
  "helpUrl": ""
}

const generator = function (block) {
  const statements = Blockly.JavaScript.statementToCode(block, "CODE");
  var code = `
    function draw() {
      ${statements}
    }
  `;
  return code;
}

export default {
  name: 'on_draw',
  view,
  generator,
}