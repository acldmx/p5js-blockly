import testBlock from "./test";
import onSetup from "./onSetup";
import line from "./line";
import ellipse from "./ellipse";
import fill from "./fill";
import clear from "./clear";
import rect from "./rect";
import rotate from "./rotate";
import random from "./random";
import onDraw from "./onDraw";
import Blockly, { Block } from "blockly";
import createVar from "./vars/createVars";
const toolbox = require("./toolbox.xml")

//配置集合文件
function loadBlock(block) {
  const { name, view, generator } = block;
  Blockly.Blocks[name] = {
    init: function () {
      this.jsonInit(view)
    }
  }

  Blockly.Blocks[name] = {
    init: function () {
      this.jsonInit(view)
    }
  }

  Blockly.JavaScript[name] = generator
}

loadBlock(testBlock);
loadBlock(onSetup);
loadBlock(onDraw);
loadBlock(line);
loadBlock(ellipse);
loadBlock(fill);
loadBlock(clear);
loadBlock(random);
loadBlock(rect);
loadBlock(rotate);

loadBlock(createVar('mouseX', "鼠标X轴位置"));
loadBlock(createVar('mouseY', "鼠标y轴位置"));
loadBlock(createVar('mouseIsPressed', "鼠标按下"));
loadBlock(createVar('width', "画布宽度"));
loadBlock(createVar('height', "画布高度"));
loadBlock(createVar('PI', "PI"));
// loadBlock(createVar('clear', "清空画布", {
//   "previousStatement": null,
//   "nextStatement": null,
// }, "clear();"));

export default {
  toolbox: toolbox,
  zoom:
  {
    controls: true,
    wheel: true,
    startScale: 1.2,
    maxScale: 2,
    minScale: 1.2,
    scaleSpeed: 1.2
  },
  move: {
    scrollbars: true,
    drag: true,
    wheel: false
  },
  grid:
  {
    spacing: 20,
    length: 1,
    colour: '#ccc',
    snap: true
  },
  trashcan: true
}