
const view = {
  "type": "fill",
  "message0": "清空画布",
  "previousStatement": null,
  "nextStatement": null,
  "colour": 230,
  "tooltip": "清空画布",
  "helpUrl": ""
}

const generator = function (block) {
  return `clear();`
}

export default {
  name: 'clear',
  view,
  generator,
}