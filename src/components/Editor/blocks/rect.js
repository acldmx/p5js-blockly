import Blockly from "blockly";  
const view = {
  "type": "rect",
  "message0": "绘制矩形 坐标X %1 坐标Y %2 宽度 %3 高度 %4",
  "args0": [
    {
      "type": "input_value",
      "name": "X",
      "check": "Number"
    },
    {
      "type": "input_value",
      "name": "Y",
      "check": "Number"
    },
    {
      "type": "input_value",
      "name": "W",
      "check": "Number"
    },
    {
      "type": "input_value",
      "name": "H",
      "check": "Number"
    }
  ],
  "inputsInline": true,
  "previousStatement": null,
  "nextStatement": null,
  "colour": 230,
  "tooltip": "",
  "helpUrl": ""
}

const generator = function (block) {
  const x = Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC);
  const y = Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC);
  const w = Blockly.JavaScript.valueToCode(block, 'W', Blockly.JavaScript.ORDER_ATOMIC);
  const h = Blockly.JavaScript.valueToCode(block, 'H', Blockly.JavaScript.ORDER_ATOMIC);

  const code = `
    rect(${x}, ${y}, ${w}, ${h});
  `;
  return code;
}

export default {
  name: 'rect',
  view,
  generator,
}