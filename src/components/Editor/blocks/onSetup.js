import Blockly from "blockly";

const view = {
  "type": "setup",
  "message0": "初始化 %1 %2",
  "args0": [
    {
      "type": "input_dummy",
      "align": "RIGHT"
    },
    {
      "type": "input_statement",
      "name": "CODE"
    }
  ],
  "colour": 230,
  "tooltip": "",
  "helpUrl": ""
}

const generator = function (block) {
  // TODO: Assemble JavaScript into code variable.
  const statements = Blockly.JavaScript.statementToCode(block, "CODE");
  var code = `
    function setup() {
      const canvas = createCanvas(500, 500);
      // canvas.parent(window.canvas);
      ${statements}
    }
  `;
  return code;
}

export default {
  name: 'on_setup',
  view,
  generator,
}