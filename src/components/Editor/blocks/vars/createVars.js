import Blockly from "blockly";

export default function createVar(varName, message, options = { "output": null }, code) {
  const view = {
    "type": varName,
    "message0": message,
    "colour": 230,
    "helpUrl": "",
    ...options
  };

  const generator = function (block) {
    if (!!code) {
      return [code, Blockly.JavaScript.ORDER_NONE]
    }
    return [varName, Blockly.JavaScript.ORDER_NONE];
  }

  return {
    name: varName,
    view,
    generator,
  }
}