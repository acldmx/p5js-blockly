
const view = {
  "type": "fill",
  "message0": "设置画笔填充色 %1",
  "args0": [
    {
      "type": "field_colour",
      "name": "COLOR",
      "colour": "#33cc00"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 230,
  "tooltip": "设置画笔填充色",
  "helpUrl": ""
}

const generator = function (block) {
  var colour_color = block.getFieldValue('COLOR');
  // TODO: Assemble JavaScript into code variable.
  var code = `fill("${colour_color}");`;
  return code;
}

export default {
  name: 'fill',
  view,
  generator,
}