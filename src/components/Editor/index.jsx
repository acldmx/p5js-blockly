import React, { createRef, useEffect, useState } from "react";
import Blockly from "blockly";
import * as CN from 'blockly/msg/zh-hans';
import BlocklyConfig from "./blocks";
import "./index.css";
import Spin from "../Spin";
// import "./toolbox.less";

export default function (props) {
  const { onCodeChange = () => { } } = props;
  let editDiv = createRef();
  const [isLoading, setLoading] = useState(false);

  const handleChange = (workspace) => {
    const code = Blockly.JavaScript.workspaceToCode(workspace);
    onCodeChange(code);
  }

  useEffect(() => {
    if (editDiv.current !== null) {
      setLoading(true);
      Blockly.setLocale(CN);
      const workspace = Blockly.inject(editDiv.current, BlocklyConfig);
      workspace.addChangeListener((e) => {
        handleChange(workspace)
        if (e.type == Blockly.Events.FinishedLoading) {
          setLoading(false);
        }
      });
      
    }
  }, []);

  return (
    <Spin spinning={isLoading}>
      <div style={{
        height: "100%"
      }} ref={editDiv}>
      </div>
    </Spin>

  );

}