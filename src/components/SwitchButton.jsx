import React from "react";
import { Button } from "@material-ui/core";
import { styled } from "@material-ui/styles";

const SwitchButton = styled(Button)({
  position: 'absolute',
  margin: 'auto',
  zIndex: 9999,
  width: '8em',
  height: '2em',
  left: 0,
  right: 0,
  boxSizing: 'border-box',
  background: 'pink',
  color: 'white',
  borderTopLeftRadius: '100px',
  borderTopRightRadius: '100px',
});

export default function SwitchButtonComponent(props) {
  const { flag, children, onClick, ...rest } = props;
  
  const handleClick = () => {
    onClick();
  }
  
  return (
    <SwitchButton 
      {...rest}
      onClick={handleClick}
      style={{
      top: flag ? 0 : "-2em",
      borderTopLeftRadius: flag ? 0 : 100,
      borderTopRightRadius: flag ? 0 : 100,
      borderBottomLeftRadius: !flag ? 0 :100,
      borderBottomRightRadius: !flag ? 0 :100,
    }}>
      {children}
    </SwitchButton>
  )
}