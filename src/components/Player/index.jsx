import React, { createRef, useEffect, useState } from "react";
import { Box, makeStyles } from "@material-ui/core";
import ToolBtns from "./ToolBtns";
import Spin from "../Spin";
const playerFrameHtml = require("./playerFrame.html")

const useStyle = makeStyles(theme => ({
  root: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'start',
    alignItems: 'center',
    boxSizing: "border-box",
    padding: '1em',
    flexDirection: 'column',
    backgroundColor: '#ecf0f1',
    height: "100%",
    width: "100%",
  },
  player: {
    width: '100%',
    maxHeight: "100%",
    maxWidth: "100%",
    boxSizing: 'border-box',
    backgroundColor: 'white',
    "& > iframe": {
      border: 'none',
    }
  },
  controls: {
    position: "absolute",
    top: "120%",
  }
}));

export default function (props) {
  const { width, height, style = {}, code } = props;
  const classes = useStyle();
  const canvas = createRef();
  const playerDiv = createRef();
  const [playerDoc, setPlayerDoc] = useState(playerFrameHtml);
  const [playerStyle, setPlayerStyle] = useState({ width, height });
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (canvas.current !== null) {
      setPlayerDoc(playerFrameHtml.replace(/\/\/!script/, code));
      setLoading(true);
    }
  }, [code]);


  useEffect(() => {
    const div = playerDiv.current;

    const r = e => {
      const pb = div.offsetWidth / width;
      const tHeight = playerStyle.height * pb;
      setPlayerStyle({
        height: tHeight > height ? tHeight : height
      })
    }

    r();
    window.addEventListener('resize', r);
  }, []);

  const onReload = () => {
    if (canvas.current !== null) {
      canvas.current.contentWindow.location.reload();
      setLoading(true);
    }
  }

  return (
    <Box className={classes.root} style={style}>
      <Spin spinning={isLoading}>
        <div ref={playerDiv} className={classes.player} style={{
          ...playerStyle,
          maxWidth: width,
          maxHeight:   height,
          overflow: 'auto'
        }}>
          <iframe onLoad={() => setLoading(false)} ref={canvas} srcDoc={playerDoc} id="canvasContainer" ref={canvas} style={{
            width: width,
            height: height
          }}></iframe>
        </div>
      </Spin>
      <ToolBtns onReload={onReload} />
    </Box>
  )
}