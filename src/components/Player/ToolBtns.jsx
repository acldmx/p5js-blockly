import React from "react";
import { Button, ButtonGroup } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { PlayArrow, Replay, Pause, Fullscreen, Cloud, CloudDownload, Height } from "@material-ui/icons"


const nullCallback = () => {}
export default function ToolBtns(props) {
  const { onReload = nullCallback } = props;

  return (
    <ButtonGroup color="primary" aria-label="outlined primary button group" style={{
      marginTop: 40
    }}>
      <Button onClick={onReload}>
        <PlayArrow></PlayArrow>
      </Button>
      <Button onClick={onReload}>
        <Replay></Replay>
      </Button>
      <Button onClick={onReload}>
        <Fullscreen></Fullscreen>
      </Button>
      <Button onClick={onReload}>
        <CloudDownload></CloudDownload>
      </Button>
      <Button onClick={onReload}>
        <Height></Height>
      </Button>
    </ButtonGroup>
  )

}