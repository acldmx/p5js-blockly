import React from "react";
import { CircularProgress } from "@material-ui/core";

export default function Spin(props) {
  const { children, spinning = true } = props;
  return (
    <div style={{
      position: 'relative',
    }}>
      {spinning && <div style={{
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        margin: '0',
        background: 'rgba(255, 255, 255, .5)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}>

        <CircularProgress />
      </div>
      }


      {children}
    </div>
  );
}