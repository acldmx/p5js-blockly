import React, { useState } from "react";
import { render } from "react-dom";
import { makeStyles, Grid, useMediaQuery, useTheme, styled, Button, Slide } from '@material-ui/core';
import Editor from './components/Editor'
import Player from "./components/Player";
import SwitchButton from "./components/SwitchButton";

const useGlobalStyles = makeStyles({
  '@global': {
    'body': {
      margin: 0,
      padding: 0,
    },
  },
});

const useStyles = makeStyles(theme => ({
  p5: {
    height: '100vh',
    overflow: 'hidden',
    position: 'relative',
  },
  editor: {
    position: 'absolute',
    top: "100%",
    left: 0,
    right: 0,
    zIndex: 100,
    width: "100%",
    height: "100%",
    transition: 'all .2s linear'
  }
}));

const App = () => {
  useGlobalStyles();
  const theme = useTheme();
  // //判断屏幕媒体决定布局
  const isSmallScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [switchFlag, setSwitchFlag] = useState(false);
  const [code, setCode] = useState("");

  const classes = useStyles({ isSmallScreen, switchFlag });
  
  const handleSwitch = () => {
    setSwitchFlag(!switchFlag)
  }

  const handleCode = (code) => {
    setCode(code);
  }

  return (
    <Grid container className={classes.p5}>
      <Grid item md={4} sm={12} xs={12}>
        <Player code={code}
          width={480}
          height={640} />
      </Grid>
      <Grid item
        md={8} sm={12} xs={12}
        className={`${isSmallScreen ? classes.editor : ''}`}
        style={{
          top: switchFlag ? 0 : "100%"
        }}>
        {isSmallScreen && <SwitchButton flag={switchFlag} onClick={handleSwitch}>Switch</SwitchButton>}
        <Editor onCodeChange={handleCode} />
      </Grid>
    </Grid>
  )

}

render(<App />, document.getElementById("root"))